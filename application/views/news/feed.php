<h2>News</h2>

<?php foreach ($news as $news_item) : ?>
	<section class="article">
		<h2 class="title"><?php echo $news_item['title'] ?></h2>
		<div class="content">
			<?php echo $news_item['text'] ?>
		</div>
		<p class="actions">
			<input type="hidden" name="article_id" value="<?php echo $news_item['id']; ?>" />
			<a href="<?php echo base_url('news') . '/' . $news_item['slug'] ?>">View article</a> | <a href="" class="editArticle">Edit Article</a>
		</p>
	</section>
<?php endforeach; ?>