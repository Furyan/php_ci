<div id="createArticleForm">
	<?php echo validation_errors(); ?>
<?php
	echo form_open(base_url('news/index'));
	$data = array('name' => 'title', 'placeholder' => 'Enter title here...', 'id' => 'title');
	echo form_input($data);
	echo '<br />';
	$data = array('name' => 'title', 'cols' => 40, 'rows' => 5, 'placeholder' => 'Enter article text here...', 'id' => 'text');
	echo form_textarea($data);
	echo '<br />';
	echo form_submit('submit', 'Create Article', 'id="submit"');
	echo form_close();
?>
	<a href="" id="cancelAction">Cancel</a>
</div>

<script type="text/javascript">
	$('#submit').click(function() {
		var form_data = {
			title: $('#title').val(),
			text: $('#text').val()
		};
		
		$.ajax({
			url: "<?php echo base_url('news/create'); ?>",
			type: 'POST',
			data: form_data,
			success: function(result) {
				$('#createArticleForm').html(result);
				$('#feed').load('news/feed');
			}
		});
		return false;
	});
	
	$('#cancelAction').click(function() {
		$('#articleAction').empty();
		return false;
	});
</script>