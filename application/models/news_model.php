<?php
class News_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->helper('url');
	}

	public function get_news($slug = FALSE, $order_by = 'DESC')
	{
		$query = $this->db->order_by("id", $order_by);
		if ($slug === FALSE)
		{
			$query = $this->db->get('news');
			return $query->result_array();
		}

		$query = $this->db->get_where('news', array('slug' => $slug));
		return $query->row_array();
	}

	public function get_news_by_id($id)
	{
		$query = $this->db->get_where('news', array('id' => $id));
		return $query->row_array();
	}
	
	public function insert_news($form_data)
	{
		$title = $form_data['title'];
		$text = $form_data['text'];

		$slug = url_title($title, 'dash', TRUE);

		$data = array(
			'title' => $title,
			'slug' => $slug,
			'text' => $text
		);

		return $this->db->insert('news', $data);
	}
	
	public function update_news($form_data)
	{
		$title = $form_data['title'];
		$text = $form_data['text'];
		$id = $form_data['article_id'];
		
		$slug = url_title($title, 'dash', TRUE);
		
		$data = array(
			'title' => $title,
			'slug' => $slug,
			'text' => $text
		);
		$this->db->where('id', $id);
		return $this->db->update('news', $data);
	}
}
