<?php
class News extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('news_model');
		$this->load->helper(array('url', 'form'));
		$this->load->library('form_validation');
	}

		public function index()
{
		$data['news'] = $this->news_model->get_news();
		$data['title'] = 'News';

		$this->load->view('templates/header', $data);
		$this->load->view('news/index', $data);
		$this->load->view('templates/footer');
	}

	public function view($slug)
	{
		$data['news_item'] = $this->news_model->get_news($slug);

		if (empty($data['news_item']))
		{
			show_404();
		}

		$data['title'] = $data['news_item']['title'];

		$this->load->view('templates/header', $data);
		$this->load->view('news/view', $data);
		$this->load->view('templates/footer');
	}
	
	public function create()
	{
			$data['title'] = 'Create a news item';

			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('text', 'Text', 'required');
			
			if ($this->form_validation->run() === FALSE) {
				$this->load->view('news/create');
			}
			else {
				$form_data = array(
					'title' => $this->input->post('title'),
					'text' => $this->input->post('text')
				);
				$this->news_model->insert_news($form_data);
				$data['actionType'] = 'Added article';
				$this->load->view('news/success', $data);
			}
	}
	
	public function update($id)
	{
		$data['title'] = 'Update a news item';
		
		$result = $this->news_model->get_news_by_id($id);
		
		echo $result['id'] . '<br/>';
		echo $result['title'] . '<br/>';
		echo $result['slug'] . '<br/>';
		echo $result['text'] . '<br/>';

/*
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');
		
		if($this->form_validation->run() === FALSE) {
			$this->load->view('news/update' . $id);
		}
		else {
			$form_data = array(
				'article_id' => $this->input->post('article_id'),
				'title' => $this->input->post('title'),
				'text' => $this->input->post('text')
			);
			$this->news_model->insert_news($form_data);
			$data['actionType'] = 'Updated article';
			$this->load->view('news/success', $data);
		}
*/
	}
	
	public function feed()
	{
		$data['news'] = $this->news_model->get_news();
		$this->load->view('news/feed', $data);
	}
}